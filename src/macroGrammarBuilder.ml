open Ast;;
open Earley;;
open Macro;;
open MacroManager;;
module DA = DynArray;;


type ('m, 't) macro_grammar_builder = {
    mmngr: 'm macro_manager;
    gram: 't grammar;
    rules_for_macro: (int, int) Hashtbl.t;
}

let create_macro_grammar_builder () :(macro_elem, Ast.ast) macro_grammar_builder =
    {
        mmngr=create_macro_manager ();
        gram=g start_symbol [| |];
        rules_for_macro=Hashtbl.create 100;
    }

let show_macro_grammar_builder bldr :string =
    show_macro_manager bldr.mmngr
    ^ "\n========== Grammar ==========\n"
    ^ str_of_grammar bldr.gram
    ^ "\n========== Macro rule indices ==========\n"
    ^ let keys = List.sort
            (fun a b -> a - b)
            (Util.hashtbl_keys bldr.rules_for_macro) in
        let f k = string_of_int k ^ ":" ^ string_of_int
            (Hashtbl.find bldr.rules_for_macro k) in
        String.concat ", " (List.map f keys)

let is_macro bldr i :bool =
    Hashtbl.mem bldr.rules_for_macro i

let ast_terminal_arr = [| t "e" (fun x -> true) |];;

let macro_to_op_fix mcr :(ast symbol) array =
    let f m :(ast symbol) =
        match m with
        | Atom a ->
                (match a with
                | Literal lit -> t (str_of_token lit) (fun x -> x = Atom lit)
                | Variable v -> n start_symbol)
        | NodeList _ -> raise (MacroErr
                "Not support list as macro pattern element")
    in
    match mcr.pattern with
    | Atom a -> raise (MacroErr "Macro pattern should be a list")
    | NodeList el -> let sub =
        (let arr = Array.of_list el in
        match mcr.fix with
        | Closed -> arr
        | Infix _ -> Array.sub arr 1 (Array.length arr - 2)
        | Prefix -> Array.sub arr 0 (Array.length arr - 1)
        | Postfix -> Array.sub arr 1 (Array.length arr - 1))
        in
        Array.map f sub
;;

let add_pgroup_rules bldr p :unit =
    let g = bldr.gram in
    let get_p_sym prefix = prefix ^ string_of_int p in
    let get_p_hat p = "P" ^ string_of_int p in
    let p_hat = get_p_hat p in
    let p_up    = get_p_sym "U" in
    let p_right = get_p_sym "R" in
    let p_left  = get_p_sym "L" in
    let p_up_arr    = [| n p_up |]    in
    let p_right_arr = [| n p_right |] in
    let p_left_arr  = [| n p_left |]  in
    let p_sym_added = Hashtbl.create 5 in
    let add_rule_g lhs rhs macro_idx :unit =
        add_rule g (r lhs rhs);
        if macro_idx >= 0 then
            let rule_idx = num_rules g - 1 in
            Hashtbl.add bldr.rules_for_macro rule_idx macro_idx
    in
    let add_rule_p_up () :unit =
        (*let p_up_added = (Hashtbl.mem p_sym_added p_up) in*)
        (*Printf.printf "p up added: %d %s\n" p (string_of_bool p_up_added);*)
        if not (Hashtbl.mem p_sym_added p_up) then begin
            List.iter
                (function
                    | 0 -> add_rule_g p_up ast_terminal_arr (-1)
                    | j -> add_rule_g p_up [| n (get_p_hat j) |] (-1))
                (get_higher_pgroups bldr.mmngr.prcdn p);
            Hashtbl.add p_sym_added p_up true
        end
    in
    let add_rule_p_hat sym :unit =
        if not (Hashtbl.mem p_sym_added sym) then begin
            add_rule_g p_hat [| n sym |] (-1);
            Hashtbl.add p_sym_added sym true
        end
    in
    let add_macro_rule imcr :unit =
        let mcr = Macro.get_macro bldr.mmngr.prcdn imcr in
        let op_fix = macro_to_op_fix mcr in
        (*let str_i = string_of_int (get_macro_index bldr.mmngr.prcdn mcr.id) in*)
        match mcr.fix with
        | Closed -> add_rule_g p_hat op_fix imcr
        | Infix Non ->
                (let rhs = Array.concat [ p_up_arr; op_fix; p_up_arr ] in
                add_rule_g p_hat rhs imcr;
                add_rule_p_up ())
        | Prefix | Infix Right -> (
                let arr = match mcr.fix with
                    | Prefix -> op_fix
                    | Infix Right -> Array.append p_up_arr op_fix
                    | _ -> assert false
                in
                add_rule_p_hat p_right;
                add_rule_g p_right (Array.append arr p_right_arr) imcr;
                add_rule_g p_right (Array.append arr p_up_arr) imcr;
                add_rule_p_up ())
        | Postfix | Infix Left -> (
                let arr = match mcr.fix with
                    | Postfix -> op_fix
                    | Infix Left -> Array.append op_fix p_up_arr
                    | _ -> assert false
                in
                add_rule_p_hat p_left;
                add_rule_g p_left (Array.append p_left_arr arr) imcr;
                add_rule_g p_left (Array.append p_up_arr arr) imcr;
                add_rule_p_up ())
    in
    add_rule_g start_symbol [| n p_hat |] (-1);
    DA.iter add_macro_rule (get_macro_indices_in_pgroup bldr.mmngr.prcdn p)
;;

let build_grammar bldr :unit =
    let _ = clear_gram bldr.gram in
    let _ = Hashtbl.clear bldr.rules_for_macro in
    let f i :unit =
        (*Printf.printf "dfs: %d\n" i;*)
        match i with
        (*precedence 0 is a special one for ast terminal*)
        | 0 -> add_rule bldr.gram (r start_symbol ast_terminal_arr)
        | _ -> add_pgroup_rules bldr i
    in
    (*add_rule bldr.gram (r start_symbol [| t (fun x -> true) |]);*)
    Macro.iter_pgroup f bldr.mmngr.prcdn;
    ()
;;


